import json

class Cmd (object):
	data    = {}

	# Being given an MQTTBase object	
	def __init__ (self, base):
		print("Creating a Cmd object.")
		print(base.msg)
		self.data = base.json
		self.data["topic"] = base.msg.topic

	def isAction (self, action):
		return self.data["action"] == action
	
	def isCmd (self, cmd):
		return self.data["cmd"] == cmd
	
	def __str__(self):
		return json.dumps(self.data);

class MQTTBase (object):
	client = None
	json   = None
	# A MQTTMessage object
	msg    = None
	# A Cmd object
	cmd    = None

	def __init__ (self, mqtt_client):
		self.client = mqtt_client

	def decode (self, msg):
		# The payload is now a JSON object?
		# payload = msg.payload.decode("UTF-8")?
		self.msg = msg
		self.json = json.loads(msg.payload)		
	
	# As long as the child classes define the flags
	# array, this seems to work. Unsure how Python3's
	# class hierarchies work that this is even possible...
	# self seems to be doing souble-duty...
	def check (self, msg):
		self.decode(msg)
		checked = False
		for f in self.flags:
			print("Checking for " + f + " in cmd")
			if f in self.json["cmd"]:
				checked = True
		return checked
	
	def respond (self, response):
		self.client.publish("commands_to_clients", str(response))

class StateHandler (MQTTBase):
	name  = "StateHandler"
	flags = ["CURRENTSTATE"]
	currentstate = "START"

	def get (self):
		if self.cmd.isCmd("CURRENTSTATE"):
			obj = { "uid"				: self.cmd.data["uid"],
							"action"		: "SET",
							"cmd"				: self.cmd.data["cmd"],
							"payload"		: { "state" : self.currentstate }}
			payload = json.dumps(obj)
			print("Responding with: ")
			print("\t" + str(payload))
			self.respond(payload)	

	def set (self):
		if self.cmd.isCmd("CURRENTSTATE"):
			self.currentstate = self.cmd.data["payload"]["state"]

	def broadcast (self):
		if self.cmd.isCmd("CURRENTSTATE"):
			obj = { "uid"				: "ALL",
							"action"		: "SET",
							"cmd"				: self.cmd.data["cmd"],
							"payload"		: { "state" : self.currentstate }}
			payload = json.dumps(obj)
			print("Responding with: ")
			print("\t" + str(payload))
			self.respond(payload)	

	def run (self, msg):
		self.decode(msg)
		# obj is an MQTTMessage where the payload
		# has been JSON decoded.
		self.cmd = Cmd(self)

		if self.cmd.isAction("GET"):
			self.get()
		elif self.cmd.isAction("SET"): 
			self.set()
		elif self.cmd.isAction("BROADCAST"):
			self.broadcast()
		else:
			print("No action for {0}".format(self.cmd))
	

