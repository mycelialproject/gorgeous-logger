from peewee import *
import datetime

db = None

class BaseModel(Model):
    class Meta:
        database = db

class Users (BaseModel):
    uid          = AutoField(primary_key = True)
    fingerprint  = TextField()
    timestamp    = DateTimeField(default = datetime.datetime.now)
    name         = TextField()

class Texts (BaseModel):
    mid          = AutoField(primary_key = True)
    fingerprint  = TextField()
    timestamp    = DateTimeField(default = datetime.datetime.now)
    tag          = TextField()
    content      = TextField()

class Questions (BaseModel):
    mid          = AutoField(primary_key = True)
    fingerprint  = TextField()
    timestamp    = DateTimeField(default = datetime.datetime.now)
    tag          = TextField()
    content      = TextField()

class DB:

	def __init__ (self):
		self.db = SqliteDatabase('mycelial.db')
    # self.db.create_tables([Users, Texts, Questions])

