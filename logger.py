import paho.mqtt.client as mqtt
from peewee import *
import datetime

db = SqliteDatabase('mycelial.db')

client = False
current_state = "START"


waits           = ["WAIT1", "WAIT2", "WAIT3", "WAIT4", "WAIT5"]
text_states     = ["NAME", "INTRODUCE", "SAME", "CONNECTED"] + waits

question_states = ["POLITICS", "MOOD", "LIFE", "COMMFEEL", "COMMPARTICIPATE"]


shrooms         = ["SHROOM1", "SHROOM2", "SHROOM3", "SHROOM4"]
flocks          = ["FLOCK1", "FLOCK2", "FLOCK3", "FLOCK4"]
prompts         = ["FINDINTRODUCE", "FINDCOMMUNITY", "MOVETOSIDE", "CHILL", "BEATDROP"]
blackouts       = [ "BLACKOUTONE", "BLACKOUTTWO", "BLACKOUTTHREE" ]

valid_states = (["STARTWAIT", "STARTTIME", "HOLD", "THEEND", "RANDOMFADE", "FINDANDSELFIE"] 
                + text_states 
                + question_states 
                + prompts
                + shrooms
                + flocks
                + blackouts
                )

class BaseModel(Model):
    class Meta:
        database = db

class Users (BaseModel):
    uid          = AutoField(primary_key = True)
    fingerprint  = TextField()
    timestamp    = DateTimeField(default = datetime.datetime.now)
    name         = TextField()

class Texts (BaseModel):
    mid          = AutoField(primary_key = True)
    fingerprint  = TextField()
    timestamp    = DateTimeField(default = datetime.datetime.now)
    tag          = TextField()
    content      = TextField()

class Questions (BaseModel):
    mid          = AutoField(primary_key = True)
    fingerprint  = TextField()
    timestamp    = DateTimeField(default = datetime.datetime.now)
    tag          = TextField()
    content      = TextField()

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    # client.subscribe("$SYS/#")
    client.subscribe("state")
    client.subscribe("set_state")
    client.subscribe("sms")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    global current_state, valid_states
    payload = msg.payload.decode("UTF-8")
    print(msg.topic + " " + payload)
    # print(msg.topic + " " + str(userdata))
    # https://stackoverflow.com/questions/606191/convert-bytes-to-a-string
    
    # If we hear something on this channel, everyone is being pushed.
    # So, we should update.
    if (msg.topic == "state"):
        if payload in valid_states:
            current_state = payload
    # This option changes the server, but does not push to the clients.        
    if (msg.topic == "set_state"):
        print("Someone is setting the state.")
        thestate = str(payload)
        if "|" in thestate:
            thestate = str(thestate.split("|")[0])
        if thestate in valid_states:
            print("Storing current state of [{0}]".format(payload))
            current_state = payload
            
    if ((msg.topic == "state") and ("WHEREAMI" in payload)):
        print("Someone is lost, but will soon be found at [{0}]".format(current_state))
        (state, fingerprint) = payload.split("|")
        client.publish("state", str(current_state) + "|" + str(fingerprint))
        # client.publish("state", current_state)
    if ((msg.topic == "sms")):
        print("TEXT MESSAGE RECEIVED")
        (whoami, tag, msg) = payload.split("|")
        print("\tFROM {0}\n\tTAG {1}\n\tMSG {2}\n".format(whoami, tag, msg))

        if (tag == "NAME"):
            q = (Users.select()                
                        .where(Users.fingerprint == whoami))
                        
            if q.count() == 0:
                u = Users.create(fingerprint = whoami, name = msg)
                try:
                    u.save()
                except Exception as e:
                    print("NAME CREATE")
                    print(e)
            else:
                u = Users.update(name = msg).where(Users.fingerprint == whoami)
                try:
                    u.execute()
                except Exception as e:
                    print("NAME UPDATE")
                    print(e)
            
        elif tag in text_states:
            try:
                m = Texts.create(fingerprint = whoami, tag = tag, content = msg)
                m.save()
            except Exception as e: 
                print("TEXT STORE")
                print(e)
        
        else:
            try:
                q = Questions.create(fingerprint = whoami, tag = tag, content = msg)
                q.save()
            except Exception as e:
                print("QUESTION STORE")
                print(e)
        
        

def mqtt_setup_and_run ():
    global client
    client = mqtt.Client(client_id="MycelialLogger2")
    client.on_connect = on_connect
    client.on_message = on_message

    # client.connect("localhost", 1883, 60)
    client.connect("localhost", 1883, 60)

    # Blocking call that processes network traffic, dispatches callbacks and
    # handles reconnecting.
    # Other loop*() functions are available that give a threaded interface and a
    # manual interface.
    client.loop_forever()

def sqlite_setup ():
    global db
    db.create_tables([Users, Texts, Questions])

# sqlite_setup()
mqtt_setup_and_run()
