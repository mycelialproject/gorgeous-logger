import paho.mqtt.client as mqtt
import datetime

from db import DB
from handlers import StateHandler

params =	{	"host" : "178.128.66.26",
						"port" : 1883,
						"channels" : ["commands_to_clients", "requests_from_clients"]
					}

machines = []

db = DB()

client = False

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
	global params
	print("Connected with result code " + str(rc))
	for chan in params["channels"]:
		print("\t... subscribing to " + chan)
		client.subscribe(chan)
		

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
		global current_state, valid_states
		# The payload is now a JSON object.
		payload = msg.payload.decode("UTF-8")
		print(msg.topic + " " + str(payload))
		if msg.topic == "requests_from_clients":  
			for m in machines:
				if m.check(msg):
					print("Machine: " + m.name)
					m.run(msg)
		else:
			print("UNHANDLED")


def initMQTT ():
	global client, params
	client = mqtt.Client()
	client.on_connect = on_connect
	client.on_message = on_message

	# client.connect("localhost", 1883, 60)
	print("Connecting to {0}:{1}".format(params["host"], params["port"]))
	client.connect(params["host"], params["port"], 60)


def initMachines ():
	global machines, client
	machines = [
		StateHandler(client)
	]

# sqlite_setup()
initMQTT()
initMachines()

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()
